// miniprogram/pages/patientDetail/patientDetail.js
const app=getApp()
var util = require('../../utils/util.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    enterDate: '2019-09-01',
    leftDate: '2019-09-01',
    patientId: null,
    templates: null,
    tname: [],
    index: -1,
    patientInfo: null,
    diagnoseInfo: [],
    templateMessageResult: null,
    first: true,
    tags: [],
    tag:null,
    formId:null,
    current:-1
  },
  tagChange:function(e){
    console.log(e.target.dataset.key)
    this.setData({
      current: e.target.dataset.key
    })
  },
  toAddTag:function(){
    wx.navigateTo({
      url: '../addTag/addTag',
    })
  },
  bindPickerChange: function(e) {
    this.setData({
      index: e.detail.value,
      diagnoseInfo: this.data.templates[e.detail.value]
    })

  },
  bindEnterDateChange: function(e) {
    this.setData({
      enterDate: e.detail.value
    })
    console.log(this.data.enterDate)
  },
  bindLeftDateChange: function(e) {
    this.setData({
      leftDate: e.detail.value
    })
    console.log(this.data.leftDate)
  },
  sendMessage:function(e){
    console.log('notice',this.data.patientInfo)

    wx.cloud.callFunction({
      // 需调用的云函数名
      name: 'notice',
      // 传给云函数的参数
      data: {
        openid: this.data.patientId,
        name: this.data.patientInfo.name,
        id: this.data.patientInfo.roomNumber,
        text: '诊断状态更新，请及时联系医生',
        formId: this.data.formId,
        templateId: 'EklC-hpnY8GykgyNcI8RbNQ0cDscQJvBUsKt3RIThBc'
      },
      // 成功回调
      //complete: console.log
      success: res => {
        console.warn('[云函数] [openapi] templateMessage.send 调用成功：', res)

        wx.showToast({
          title: '发送成功',
        })
        this.setData({
          templateMessageResult: JSON.stringify(res.result)
        })
        console.log(this.data.templateMessageResult)
      },
      fail: err => {
        wx.showToast({
          icon: 'none',
          title: '调用失败',
        })
        console.error('[云函数] [openapi] templateMessage.send 调用失败：', err)
      }
    })
  },
  onUpdate: function(e) {
    var that=this
    console.log(app.globalData.patientInfo)
    const db = wx.cloud.database()
    if (this.data.first) {
      db.collection('diagnose').add({
        data: {
          patientId: this.data.patientId,
          physicianId: app.globalData.physicianInfo._id,
          diagnose: e.detail.value.diagnose,
          surgery: e.detail.value.surgery,
          treatment: e.detail.value.treatment,
          follow: e.detail.value.follow,
          enterDate: this.data.enterDate,
          leftDate: this.data.leftDate,
          tag:this.data.tags[this.data.current]
        },
        success: res => {
          console.log('success', res)
          that.sendMessage(e)
          wx.reLaunch({
            url: '../physicians/physicians',
          })
        },
        fail: err => {
          wx.showToast({
            icon: 'none',
            title: '调用失败',
          })
          console.error('[云函数] [openapi] templateMessage.send 调用失败：', err)
        }
      })
    } else {
      db.collection('diagnose').doc(this.data.diagnoseId).update({
        data: {
          patientId: this.data.patientId,
          physicianId: app.globalData.physicianInfo._id,
          diagnose: e.detail.value.diagnose,
          surgery: e.detail.value.surgery,
          treatment: e.detail.value.treatment,
          follow: e.detail.value.follow,
          enterDate: this.data.enterDate,
          leftDate: this.data.leftDate,
          tag: this.data.tags[this.data.current]
        },
        success: res => {
          console.log('success', res)
          that.sendMessage(e)
          wx.reLaunch({
            url: '../physicians/physicians',
          })
        },
        fail: err => {
          wx.showToast({
            icon: 'none',
            title: '调用失败',
          })
          console.error('[云函数] [openapi] templateMessage.send 调用失败：', err)
        }
      })
    }
  },
  enterDateChange: function(e) {
    this.setData({
      enterDate: e.detail.value
    })
  },
  leaveDateChange: function(e) {
    this.setData({
      leaveDate: e.detail.value
    })
  },
  toTemplate: function() {
    wx.navigateTo({
      url: '../chooseTemplate/chooseTemplate',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that=this
    this.setData({
      patientId: options.openid,
      tags:getApp().globalData.tags
    })
    const db = wx.cloud.database()
    db.collection('formId').where({
      _openid:that.data.patientId
    }).get({
      success:res=>{
        that.setData({
          formId:res.data[0].formId
        })
      }
    })
    db.collection('patients').where({
      _openid: that.data.patientId
    }).get({
      success: res => {
        console.log(res.data)
        this.setData({
          patientInfo: res.data[0]
        })
      }
    })
    db.collection('diagnose').where({
      patientId: that.data.patientId
    }).get({
      success: res => {
        console.log(res.data)
        if (res.data.length > 0) {
          this.setData({
            diagnoseInfo: res.data[0],
            diagnoseId: res.data[0]._id,
            first: false
          })
        }
        for(let i=0;i<that.data.tags.length;i++){
          if (that.data.tags[i] == that.data.diagnoseInfo.tag){
            that.setData({
              current:i
            })
            break
          }
        }
        console.log('current:',that.data.current)
      },
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.setData({
      tags: getApp().globalData.tags
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})