// miniprogram/pages/authorize/authorize.js
const app=getApp()
var util = require('../../utils/util.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },
  onGetOpenid: function (e) {
    // 调用云函数
    console.log(e)
    wx.cloud.callFunction({
      name: 'login',
      data: {},
      success: res => {
        app.globalData.openid = res.result.openid
        console.log('[云函数] [login] user openid: ', app.globalData.openid)
        app.globalData.hasAuthorize=true
        console.log('hasAuthorize:',app.globalData.hasAuthorize)
        const db = wx.cloud.database()
        db.collection('formId').where({
          _openid: app.globalData.openid
        }).get({
          success:res=>{
            if(res.data.length>0){
              db.collection('formId').doc(res.data[0]._id).update({
                data:{
                  formId:e.detail.formId
                },
                success:res=>{
                  console.log('fid更新成功')
                }
              })
            }else{
              db.collection('formId').add({
                data:{
                  formId: e.detail.formId
                },
                success: res => {
                  console.log('fid新增成功')
                }
              })
            }
          }
        })
        wx.redirectTo({
          url: '../home/home',
        })
      },
      fail: err => {
        console.error('[云函数] [login] 调用失败', err)
      }
    })

    
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(app.globalData.hasAuthorize)
    if (app.globalData.hasAuthorize){
      wx.reLaunch({
        url: '../home/home',
      })
    }

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})