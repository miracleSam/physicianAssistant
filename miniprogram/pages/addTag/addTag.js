// miniprogram/pages/addTag/addTag.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tags: [],
    _id: null

  },

  /**
   * 生命周期函数--监听页面加载
   */
  submitForm: function (e) {
    var that = this
    this.setData({
      tags: this.data.tags.concat(e.detail.value.tag)
    });
    const db = wx.cloud.database()
    if (this.data._id == null) {
      db.collection('tags').add({
        data: {
          tags: that.data.tags
        },
        success: res => {
          console.log('添加成功')
          getApp().globalData.tags = that.data.tags
          wx.showToast({
            title: '添加成功',
          })
          // wx.redirectTo({
          //   url: '../myPatients/myPatients',
          // })
          wx.navigateBack()
        }
      })
    } else {
      db.collection('tags').doc(this.data._id).update({
        data: {
          tags: that.data.tags
        },
        success: res => {
          console.log('更新成功')
          getApp().globalData.tags = that.data.tags
          wx.showToast({
            title: '添加成功',
          })
          // wx.redirectTo({
          //   url: '../myPatients/myPatients',
          // })
          wx.navigateBack()
        },
        fail: err => {
          console.log(err)
        }
      })
    }
  },
  onLoad: function (options) {
    const db = wx.cloud.database()
    db.collection('tags').where({
      _openid: getApp().globalData._openid
    }).get({
      success: res => {
        console.log('tags', res.data)
        this.setData({
          _id: res.data[0]._id,
          tags: res.data[0].tags
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})