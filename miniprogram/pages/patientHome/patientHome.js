// miniprogram/pages/patientHome/patientHome.js
const app=getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    patientInfo: null,
    diagnoseInfo: null,
    formId:null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  callPhysician: function(e) {
    console.log(e)
    wx.cloud.callFunction({
      // 需调用的云函数名
      name: 'send',
      // 传给云函数的参数
      data: {
        openid:this.data.patientInfo.physicianId,
        name: this.data.patientInfo.name,
        mobile: this.data.patientInfo.mobile,
        text:'病患呼叫医生，请及时处理',
        formId:this.data.formId,
        templateId:'6oUxOo39TF1YDhOM-YGkCkBgptknpSQdwfD2p1dg3HM'
      },
      // 成功回调
      //complete: console.log
      success: res => {
        console.warn('[云函数] [openapi] templateMessage.send 调用成功：', res)

        wx.showToast({
          title: '发送成功',
        })
        this.setData({
          templateMessageResult: JSON.stringify(res.result)
        })
        console.log(this.data.templateMessageResult)
      },
      fail: err => {
        wx.showToast({
          icon: 'none',
          title: '调用失败',
        })
        console.error('[云函数] [openapi] templateMessage.send 调用失败：', err)
      }
    })
  },
  refreshInfo:function(){
    const db = wx.cloud.database()
    db.collection('patients').where({
      _openid: app.globalData.openid
    }).get({
      success: res => {
        if (res.data.length > 0) {
          console.log('刷新结果：', res.data)
          app.globalData.patientInfo = res.data[0]
          this.setData({
            patientInfo: res.data[0]
          })
          wx.showToast({
            title: '刷新成功',
          })
        } else {
          app.globalData.isPatient = false
        }
      },
    })
  },
  onLoad: function(options) {
    this.setData({
      patientInfo: getApp().globalData.patientInfo
    })
    const db = wx.cloud.database()
    db.collection('diagnose').where({
      patientId: this.data.patientInfo._openid
    }).get({
      success: res => {
        console.log(res.data)
        this.setData({
          diagnoseInfo:res.data[0]
        })
      },
    })
    db.collection('formId').where({
      _openid:this.data.patientInfo.physicianId
    }).get({
      success:res=>{
        this.setData({
          formId:res.data[0].formId
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})