// miniprogram/pages/tags/tags.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tags:[],
    slideButtons: [{
      type: 'warn',
      text: '删除',
      extClass: 'test',
      src: '', // icon的路径
    }],

  },
  slideButtonTap(e) {
    console.log('slide button tap', e.target.dataset.index)
    var index = e.target.dataset.index
    var tags=getApp().globalData.tags
    var t=[]
    var j = 0
    for(var i=0;i<tags.length;i++){
      if(i==index) continue
      t[j]=tags[i]
      j++
    }
    var that = this
    wx.showModal({
      title: '删除提醒',
      content: '确认删除此条标签吗？',
      success(res) {
        if (res.confirm) {
          console.log('用户点击确定')
          const db = wx.cloud.database()
          db.collection('tags').doc(getApp().globalData.tid).update({
            data:{
              tags:t
            },
            success:res=> {
              wx.showToast({
                title: '删除成功'
              })
              console.log(t)
              getApp().globalData.tags=t
              that.setData({
                tags:t
              })
              console.log('local:',that.data.tags)
            }
          })
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    //console.log(getApp().globalData.tags)
    this.setData({
      tags:getApp().globalData.tags
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})