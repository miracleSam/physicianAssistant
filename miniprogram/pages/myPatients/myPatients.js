// miniprogram/pages/myPatients/myPatients.js
const app = getApp()
var util = require('../../utils/util.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    winHeight: "",//窗口高度
    currentTab: 0, //预设当前项的值
    scrollLeft: 0, //tab标题的滚动条位置
    patients:null,
    physicianInfo:null,
    id:null,
    currentTab: '0',
    isSelects: true,
    tabChange:true,
    tags:[],
    globalTags:[],
    patientTags:[],
    list: [{
      text: "我的患者",
      iconPath: "/images/patientS.png",
      selectedIconPath: "/images/patient.png",
      badge: 'New'
    },
    {
      text: "我的资料",
      iconPath: "/images/physicianS.png",
      selectedIconPath: "/images/physician.png"
    }]
  },
  // 滚动切换标签样式
  switchTab: function (e) {
    this.setData({
      currentTab: e.detail.current
    });
    this.checkCor();
  },
  // 点击标题切换当前页时改变样式
  swichNav: function (e) {
    console.log(e.target.dataset.current)
    var cur = e.target.dataset.current;
    if (this.data.currentTaB == cur) { return false; }
    else {
      this.setData({
        currentTab: cur
      })
    }
  },
  //判断当前滚动超过一屏时，设置tab标题滚动条。
  checkCor: function () {
    if (this.data.currentTab > 4) {
      this.setData({
        scrollLeft: 300
      })
    } else {
      this.setData({
        scrollLeft: 0
      })
    }
  },
  tabChange(e) {
    //console.log('tab change', e)
    var n=this.data.tabChange
    this.setData({
      tabChange:!n
    })
    //console.log(this.data.physicianInfo)
    if(this.data.physicianInfo==null){
      this.getInfo()
    }
  },
  toTags:function(){
    wx.navigateTo({
      url: '../tags/tags',
    })
  },
  toEditor:function(e){
    //console.log(e.currentTarget.dataset)
    wx.navigateTo({
      url: '../patientDetail/patientDetail?openid=' + e.currentTarget.dataset.openid,
    })
  },
  refreshPatients:function(){
    util.getPatients()
    this.setData({
      patients: app.globalData.myPatients,
    })
    wx.showToast({
      title: '刷新成功',
    })

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log('mypatients', app.globalData.myPatients)
    this.setData({
      physicianInfo: app.globalData.physicianInfo,
      patients:app.globalData.myPatients,
      tags:app.globalData.tags
    })


    var that = this;
    //  高度自适应
    wx.getSystemInfo({
      success: function (res) {
        var clientHeight = res.windowHeight,
          clientWidth = res.windowWidth,
          rpxR = 750 / clientWidth;
        var calc = clientHeight * rpxR - 180;
        console.log(calc)
        that.setData({
          winHeight: calc
        });
      }
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.setData({
      tags: getApp().globalData.tags,
      patients: app.globalData.myPatients,
    })
    //console.log('pp',this.data.patients)
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})