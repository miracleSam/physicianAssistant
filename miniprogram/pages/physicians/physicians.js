// miniprogram/pages/physicians/physicians.js
const app = getApp()
var util = require('../../utils/util.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    hasRegister: false,
    showTopTips: false,
    error: '',

    radioItems: [{
        name: '男',
        value: 'male'
      },
      {
        name: '女',
        value: 'female'
      }
    ],
    formData: {

    },
    rules: [{
        name: 'radio',
        rules: {
          required: true,
          message: '请选择性别'
        },
      }, 
      {
        name: 'name',
        rules: {
          required: true,
          message: '请输入姓名'
        },
      },
      {
        name: 'id',
        rules: {
          required: true,
          message: '请输入工号'
        },
      },
      {
        name: 'department',
        rules: {
          required: true,
          message: '请输入科室'
        },
      }, { // 多个规则
        name: 'mobile',
        rules: [{
          required: true,
          message: '请输入手机号'
        }, {
          mobile: true,
          message: '手机号格式不对'
        }],
      },
      //  {
      //   name: 'vcode',
      //   rules: { required: true, message: '验证码必填' },
      // }, 
      {
        name: 'idcard',
        rules: {
          required: true,
          message: '请输入身份证'
        },
      }
    ]
  },
  radioChange: function(e) {
    //console.log('radio发生change事件，携带value值为：', e.detail.value);
    this.setData({
      gender: e.detail.value
    })
    console.log(this.data.gender)
    var radioItems = this.data.radioItems;
    for (var i = 0, len = radioItems.length; i < len; ++i) {
      radioItems[i].checked = radioItems[i].value == e.detail.value;
    }

    this.setData({
      radioItems: radioItems,
      [`formData.radio`]: e.detail.value
    });
  },
  formInputChange(e) {
    const {
      field
    } = e.currentTarget.dataset
    this.setData({
      [`formData.${field}`]: e.detail.value
    })
  },
  submitForm: function(e) {
    //console.log(e.detail.value)
    this.selectComponent('#form').validate((valid, errors) => {
      console.log('valid', valid, errors)
      if (!valid) {
        // const firstError = Object.keys(errors)
        // console.log(firstError)
        for (let i = 0; i < errors.length; i++) {
          if (errors[i].message != null)
            this.setData({
              error: errors[i].message
            })
        }
      }
      if (!valid) {
        const firstError = Object.keys(errors)
        if (firstError.length) {
          this.setData({
            error: errors[firstError[0]].message
          })

        }
      } else {
        wx.showToast({
          title: '校验通过'
        })
        this.onAdd(e)
      }
    })
  },
  onAdd: function(e) {
    console.log(e.detail.value)
    const db = wx.cloud.database()
    db.collection('physicians').add({
      data: {
        name: e.detail.value.name,
        id: e.detail.value.id,
        department: e.detail.value.department,
        mobile: e.detail.value.mobile,
        idCard: e.detail.value.idCard,
        gender: this.data.gender,
        formId:e.detail.formId
      },
      success: res => {
        // 在返回结果中会包含新创建的记录的 _id
        this.setData({
          counterId: res._id,
          count: 1
        })
        wx.showToast({
          title: '注册成功',
          icon: 'success'
        })
        wx.reLaunch({
          url: '../home/home',
        })
        console.log('[数据库] [新增记录] 成功，记录 _id: ', res._id)
      },
      fail: err => {
        wx.showToast({
          icon: 'none',
          title: '新增记录失败'
        })
        console.error('[数据库] [新增记录] 失败：', err)
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    if (app.globalData.isPhysician) {
      util.getPatients()
      wx.reLaunch({
        url: '../myPatients/myPatients',
      })
    }

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})