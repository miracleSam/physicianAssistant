// miniprogram/pages/patients/patients.js
const app = getApp()
var util = require('../../utils/util.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    departments: [],
    // enterDate: '2019-09-01',
    // leftDate: '2019-09-01',
    index: 0,
    queryResult: null,
    gender: null,
    radioItems: [{
        name: '男',
        value: 'male'
      },
      {
        name: '女',
        value: 'female'
      }
    ],
    formData: {

    },
    rules: [{
        name: 'radio',
        physicianId: null,
        rules: {
          required: true,
          message: '请选择性别'
        },
      }, {
        name: 'name',
        rules: {
          required: true,
          message: '请输入姓名'
        },
      },
      {
        name: 'age',
        rules: {
          required: true,
          message: '请输入年龄'
        },
      },
      {
        name: 'roomNumber',
        rules: {
          required: true,
          message: '请输入病房号'
        },
      }, {
        name: 'id',
        rules: {
          required: true,
          message: '请输入医生工号'
        },
      }, { // 多个规则
        name: 'mobile',
        rules: [{
          required: true,
          message: '请输入手机号'
        }, {
          mobile: true,
          message: '手机号格式不对'
        }],
      },
      //  {
      //   name: 'vcode',
      //   rules: { required: true, message: '验证码必填' },
      // }, 
      {
        name: 'idcard',
        rules: {
          required: true,
          message: '请输入身份证'
        },
      }
    ]
  },

  radioChange: function(e) {
    console.log('radio发生change事件，携带value值为：', e.detail.value);
    this.setData({
      gender: e.detail.value
    })
    console.log(this.data.gender)
    var radioItems = this.data.radioItems;
    for (var i = 0, len = radioItems.length; i < len; ++i) {
      radioItems[i].checked = radioItems[i].value == e.detail.value;
    }

    this.setData({
      radioItems: radioItems,
      [`formData.radio`]: e.detail.value
    });
  },
  formInputChange(e) {
    const {
      field
    } = e.currentTarget.dataset
    this.setData({
      [`formData.${field}`]: e.detail.value
    })
  },
  submitForm: function(e) {
    //console.log(e.detail.value)
    var that=this
    this.selectComponent('#form').validate((valid, errors) => {
      console.log('valid', valid, errors)
      if (!valid) {
        for (let i = 0; i < errors.length; i++) {
          if (errors[i].message != null) {
            this.setData({
              error: errors[i].message
            })
          }
        }
      } else {
        const db = wx.cloud.database()
        db.collection('physicians').where({
          name: e.detail.value.id
        }).get({
          success: res => {
            if (res.data.length > 0) {
              that.setData({
                physicianId: res.data[0]._openid
              })
              wx.showToast({
                title: '校验通过'
              })
              that.onAdd(e)
            }else{
              that.setData({
                error: '无此医生'
              })
            }
          }
        })

      }
    })
  },
  onAdd: function(e) {
    //console.log(e.detail.value)
    var time = util.formatTime(new Date());
    console.log(time)
    const db = wx.cloud.database()
    db.collection('patients').add({
      data: {
        name: e.detail.value.name,
        gender: this.data.gender,
        mobile: e.detail.value.mobile,
        age: e.detail.value.age,
        roomNumber: e.detail.value.roomNumber,
        idCard: e.detail.value.idCard,
        id: e.detail.value.id,
        physicianId: this.data.physicianId,
        time: time
        // enterDate:this.data.enterDate,
        // leftDate:this.data.leftDate
      },
      success: res => {
        // 在返回结果中会包含新创建的记录的 _id
        this.setData({
          counterId: res._id,
          count: 1
        })
        wx.showToast({
          title: '注册成功',
        })
        wx.reLaunch({
          url: '../home/home',
        })
        console.log('[数据库] [新增记录] 成功，记录 _id: ', res._id)
      },
      fail: err => {
        wx.showToast({
          icon: 'none',
          title: '新增记录失败'
        })
        console.error('[数据库] [新增记录] 失败：', err)
      }
    })

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    console.log('isPatient:', app.globalData.isPatient)
    if (app.globalData.isPatient) {
      wx.reLaunch({
        url: '../patientHome/patientHome',
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})