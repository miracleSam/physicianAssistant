const app = getApp()

function formatTime(date) {
  var year = date.getFullYear()
  var month = date.getMonth() + 1
  var day = date.getDate()

  var hour = date.getHours()
  var minute = date.getMinutes()
  var second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

function formatNumber(n) {
  n = n.toString()
  return n[1] ? n : '0' + n
}

function isPhysicians() {
  console.log('start', app.globalData.openid)
  const db = wx.cloud.database()
  db.collection('physicians').where({
    _openid: app.globalData.openid
  }).get({
    success: res => {
      if (res.data.length > 0) {
        app.globalData.isPhysician = true
        app.globalData.physicianInfo = res.data[0]
        console.log('医生查询结果：', res.data)
      } else {
        app.globalData.isPatient = false
      }
    },
  })
}

function isPatients() {
  const db = wx.cloud.database()
  db.collection('patients').where({
    _openid: app.globalData.openid
  }).get({
    success: res => {
      if (res.data.length > 0) {
        console.log('病人查询结果：', res.data)
        app.globalData.isPatient = true
        app.globalData.patientInfo = res.data[0]
      } else {
        app.globalData.isPatient = false
      }
    },
  })
}

function getPatients() {
  const db = wx.cloud.database()
  var p = app.globalData.physicianInfo
  db.collection('patients').where({
    id: p.name
  }).orderBy('time', 'desc').get({
    success: res => {
      var p= res.data
     // var p = app.globalData.myPatients
      for (let index = 0; index < p.length; index++) {
        db.collection('diagnose').where({
          patientId: p[index]._openid
        }).get({
          success: res => {
            p[index].tag = res.data[0].tag
            p[index].leftDate = res.data[0].leftDate
          },
        })

      }
      app.globalData.myPatients=p
      console.log('我的患者:', app.globalData.myPatients)
    },
    fail: err => {
      wx.showToast({
        icon: 'none',
        title: '查询记录失败'
      })
      console.error('[数据库] [查询记录] 失败：', err)
    }
  })
}

function getTags() {
  const db = wx.cloud.database()
  db.collection('tags').where({
    _openid: app.globalData.openid
  }).get({
    success: res => {
      console.log(res.data)
      if (res.data.length > 0) {
        //app.globalData.tags.concat(res.data[0].tags)
        app.globalData.tid=res.data[0]._id,
        app.globalData.tags = res.data[0].tags
        console.log('我的标签: ', app.globalData.tags)
      }
    },
    fail: err => {
      wx.showToast({
        icon: 'none',
        title: '查询记录失败'
      })
      console.error('[数据库] [查询记录] 失败：', err)
    }
  })

}
module.exports = {
  formatTime: formatTime,
  isPatients: isPatients,
  isPhysicians: isPhysicians,
  getPatients: getPatients,
  getTags: getTags

}