# 医患助手 physicianAssistant 小程序云开发
# 云开发 quickstart

这是云开发的快速启动指引，其中演示了如何上手使用云开发的三大基础能力：

- 数据库：一个既可在小程序前端操作，也能在云函数中读写的 JSON 文档型数据库
- 文件存储：在小程序前端直接上传/下载云端文件，在云开发控制台可视化管理
- 云函数：在云端运行的代码，微信私有协议天然鉴权，开发者只需编写业务逻辑代码

## 参考文档

- [云开发文档](https://developers.weixin.qq.com/miniprogram/dev/wxcloud/basis/getting-started.html)

## 运行界面
![输入图片说明](https://images.gitee.com/uploads/images/2019/1105/162559_68e9d985_1515186.png "TIM截图20191105162550.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1105/162657_f8bec7de_1515186.png "TIM截图20191105162524.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1105/162710_53b434b5_1515186.png "TIM截图20191105162506.png")
![云函数模板消息](https://images.gitee.com/uploads/images/2019/1105/162741_8ed14c38_1515186.jpeg "TIM图片20191105162733.jpg")
![云数据库](https://images.gitee.com/uploads/images/2019/1105/162831_0a83878a_1515186.png "TIM截图20191105162824.png")